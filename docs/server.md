# Server Configuration

In this tutorial we learn how to install **OS** on a `Bare Metal` or `Virtualized` hardware, and install our dependencies

There are five main steps to install all requirements

1. OS
2. Docker
3. Sentry
4. Runner

---

## OS

Install any operation system on your hardware, i recommend to install a light weight OS, only for installing docker host

1. Ubuntu Server
2. Ubuntu Core
3. CoreOS
4. RancherOS

---

### Proxy

Fortunately i live in iran but unfortunately external limitations and internal stupidity forced us to be a law breaker to speed up our progress

There are two main ways for breaking these laws

1. **Shecan/Begzar DNS**
2. **Socks Proxy**

#### Shecan/Begzar

We can configure [shecan](https://shecan.ir) or [begzar](https://begzar.ir) DNS server to use docker or gitlab or etc

1. systemd-resolved
2. resolvconf

##### systemd-resolved

-   Add **shecan** or **begzar** DNS servers to `/etc/systemd/resolved.conf`

```code
[Resolve]
DNS=185.51.200.2 178.22.122.100
```

-   Remove old `resolv.conf` file and add systemd link

```code
sudo rm -f /etc/resolv.conf
sudo ln -s /run/systemd/resolve/resolv.conf /etc/resolv.conf
```

-   Restart `systemd-resolved`

```code
sudo service systemd-resolved restart
```

---

##### resolvconf

-   Install `resolvconf`

```code
sudo apt install resolvconf
```

-   Add **shecan** or **begzar** DNS servers to `/etc/resolvconf/resolv.conf.d/head`

```code
nameserver 185.51.200.2
nameserver 178.22.122.100
```

-   Restart `resolvconf`

```code
sudo service resolvconf restart
```

---

#### Socks

-   Run your stack with a `chisel` proxy inside containers with `docker-compose up -d`:

```yml
version: "3.5"
services:
    runner:
        image: gitlab/gitlab-runner
        restart: always
        environment:
            HTTP_PROXY: socks5://proxy:1080
            HTTPS_PROXY: socks5://proxy:1080
        volumes:
            - ./config.toml:/etc/gitlab-runner/config.toml
            - /var/run/docker.sock:/var/run/docker.sock
            - /home/ubuntu/.docker/machine/machines:/etc/gitlab-runner/certs
    proxy:
        image: jpillora/chisel
        restart: always
        command: client https://banana-market-koliber.fandogh.cloud/ 0.0.0.0:socks
        ports:
            - 1080:1080
    gateway:
        image: nginx:alpine
        restart: always
        volumes:
            - ./certificate.crt:/etc/ssl/certificate.crt
            - ./private.key:/etc/ssl/private.key
            - ./nginx.conf:/etc/nginx/nginx.conf
        networks:
            - default
            - my_app
        ports:
            - "80:80"
            - "443:443"

networks:
    default:
        name: stack
    my_app:
        external:
            name: my_app_default
```

-   Edit file `/etc/systemd/system/docker.service.d/http-proxy.conf`

```conf
[Service]
Environment="HTTP_PROXY=socks5://proxy:1080/"
Environment="HTTPS_PROXY=socks5://proxy:1080/"
```

-   Edit file `~/.docker/config.json`

```json
{
    "proxies": {
        "default": {
            "httpProxy": "socks5h://proxy:1080",
            "httpsProxy": "socks5h://proxy:1080"
        }
    }
}
```

-   Edit file `/etc/hosts`

```txt
{server_ip} proxy
```

-   Run commands bellow

```bash
sudo systemctl daemon-reload
sudo systemctl restart docker
```

---

## Docker

Now we must install **Docker Host** using these commands

```code
sudo apt install docker.io
sudo usermod -aG docker $USER
sudo docker login
```

---

## Sentry

Issue tracking is one of the most important parts of continous monitoring, i use sentry for issue tracking in my products

For installing this software using docker see [this](https://github.com/getsentry/onpremise) link

```code
git clone https://github.com/getsentry/onpremise
cd onpromise
./install.sh
```

---

## Runner

The final step is to install runner for automating **build**, **test**, **deploy** steps (`CI/CD`)

Runner is a server configured to run jobs

Runners can run commands in Docker or Shell or etc

See [this](https://docs.gitlab.com/runner/) link to follow how to install gitlab-runner on your server

I recommend you to install your gitlab runner as a docker service

```code
docker run -d --name gitlab-runner --restart always \
    -v /srv/gitlab-runner/config:/etc/gitlab-runner \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v /home/ubuntu/.docker/machine/machines:/etc/gitlab-runner/certs \
    gitlab/gitlab-runner:latest
```

<!--prettier-ignore-->
!!! tip "Raspberry Pi 2 Runner"
    For installing gitlab runner on `Raspberry Pi 2`, use `klud/gitlab-runner:latest` image instead

<!--prettier-ignore-->
!!! tip "Docker-Machine certs path"
    If you want to run on another docker machine change `/home/ubuntu/.docker/machine/machines` to your **docker-machine** certs path

Now you must register your runner for a **Group** or **Sub Group** or **Project**

```code
docker run --rm -it -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register
```

---

### Tags

Every runner have some tags, this tags used to selecting the runner by jobs to run using `tags` property

We can specify which runner to run this job using job tags, matches a specific runner tags

<!--prettier-ignore-->
!!! tip "Untagged Runners"
    A runner can run untagged job, we must check this checkbox on runner configs

    ![Runner](assets/gitlab_runner.png)

---

### Proxy

We must configure our proxy settings in runner to set them before running jobs

There are two main ways for using proxy

1. **Shecan/Begzar DNS**
2. **Socks Proxy**

---

#### Shecan/Begzar

-   Edit file `/srv/gitlab-runner/config/config.toml`

```toml
[[runners]]
  # ...
  [runners.docker]
    # ...
    dns = ["185.51.200.2", "178.22.122.100"]
    dns_search = ["185.51.200.2", "178.22.122.100"]
    # ...
```

---

#### Socks

-   Edit file `/srv/gitlab-runner/config/config.toml`

```toml
[[runners]]
    # ...
    pre_clone_script = "git config --global http.proxy $HTTP_PROXY; git config --global https.proxy $HTTPS_PROXY"
    environment = ["HTTPS_PROXY=socks5://proxy:1080", "HTTP_PROXY=socks5://proxy:1080", "NO_PROXY=registry.npmjs.org"]
    # ...
```

---

### Docker Socket

By default our runner will create new docker host, we can bind our host docker to the runner to prevent this

-   Edit file `/srv/gitlab-runner/config/config.toml`

```toml
[[runners]]
  # ...
  [runners.docker]
    # ...
    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
    # ...
```

---

### Docker Host

We can use another docker host socket, for example, our runner registered on **Raspberry Pi** client and our docker containers will run on **ESXi** hypervisor:

-   Install **docker**, **docker-machine** on rpi from docker repo

-   Create machine on ESXi using **docker-machine**:

```bash
export VSPHERE_CPU_COUNT=4
export VSPHERE_MEMORY_SIZE=12288
export VSPHERE_DISK_SIZE=307200
export VSPHERE_VCENTER="192.168.100.100"
export VSPHERE_USERNAME="root"
export VSPHERE_PASSWORD="{P@ssw0rd...}021"
export VSPHERE_NETWORK="VM Traffic"
export VSPHERE_DATASTORE="datastore1"

docker-machine create -d vmwarevsphere Docker
```

-   Configure **Static IP**, **DNS Servers** on Boot2Docker: (`/var/lib/boot2docker/bootlocal.sh`)

```bash
# configure IP
kill `cat /var/run/udhcpc.eth0.pid`
ifconfig eth0 192.168.100.101 netmask 255.255.255.0 up
ip route add default via 192.168.100.1 dev eth0

# configure DNS
cat << EOF > /etc/resolv.conf
nameserver 185.51.200.2
nameserver 178.22.122.100
nameserver 192.168.100.1
```

-   Edit **config.toml** and add **ESXi** docker socket:

```toml
[[runners]]
  # ...
  [runners.docker]
    # ...
    host = "tcp://192.168.100.101:2376"
    tls_verify = true
    tls_cert_path = "/etc/gitlab-runner/certs/Docker"
    # ...
    dns = ["185.51.200.2", "178.22.122.100"]
    dns_search = ["185.51.200.2", "178.22.122.100"]
    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
    # ...
```

---

#### Docker Socket Binding

Bind docker client to another docker server will complete in there steps:

1. Enable TLS (HTTPS)
2. Set certificate path
3. Change Docker host

We can do these steps using ways bellow:

-   **Docker Machine**:

```bash
eval `docker-machine env Docker`
```

-   **Environment Variables**:

```bash
export DOCKER_TLS_VERIFY="1"
export DOCKER_HOST="tcp://192.168.100.101:2376"
export DOCKER_CERT_PATH="/home/ubuntu/.docker/machine/machines/Docker"
```

-   **Gitlab Runner**:

```toml
[[runners]]
# ...
[runners.docker]
    # ...
    host = "tcp://192.168.100.101:2376"
    tls_verify = true
    tls_cert_path = "/etc/gitlab-runner/certs/Docker"
    # ...
```

---

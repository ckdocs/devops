# Gitlab

Gitlab mainly is a public repository provider for **git**, so it will provide:

-   **Continuous Development**

The new features of gitlab are:

-   **Continuous Testing**
-   **Continuous Integration**
-   **Continuous Deployment**

Provided in **Gitlab CI/CD**

In the previous document, we introduced how to install **Gitlab Runner** and registering it for **Groups** or **Sub Groups** or **Projects**

Now we learn about **CI/CD** configuration files

---

## `.gitlab-ci.yml`

Gitlab CI/CD is configured using a file named `.gitlab-ci.yml` placed in the repository's root

This file contains some commands executed by a `Gitlab Runner`

1. create file in repository's root
2. define jobs for `build`, `test`, `deploy`
    1. define `only/...` conditions to run job
    2. define `branch` listeners for job
    3. define `tags` for runner selection
    4. define `scripts` for job
3. define jobs sequence in `parallel` or `sequential`
4. push file for every commit

---

### Concepts

There are some main concepts in every `.gitlab-ci.yml` file:

1. **Script**: A terminal command
2. **Job**: A group of scripts
3. **Stage**: A job or many parallel jobs
4. **Pipeline**: A sequence of stages to `build`, `test`, `deploy` application
5. **Runner**: A server to run a job

#### Script

Every terminal command called a script

**Example**:

-   `npm install`
-   `npm run build`
-   `npm run test`
-   `npm start`
-   `docker-compose up`

---

#### Job

Is a group of scripts with some other configurations about **when/how/where** to run the job

**Example**:

```yml
job_build:
    stage: build
    script:
        - npm install
        - npm run build
```

<!--prettier-ignore-->
!!! tip "Job Logs"
    We can see the outputs of executing a job on gitlab

    ![Job Logs](assets/gitlab_job.png)

---

#### Stage

Is a group of parallel jobs or one job runs sequentially

![Stage](assets/gitlab_stages.png)

-   `1 parallel job` on stage `Upstream`
-   `5 parallel jobs` on stage `Tests`
-   `1 parallel job` on stage `Post tests`
-   `2 parallel jobs` on stage `Downstream`

**Example**:

```yml
job_test_1:
    stage: test
    scripts:
        - npm run test

job_test_2:
    stage: test
    scripts:
        - stress -t node
```

<!--prettier-ignore-->
!!! tip "Parallel Job"
    Parallel jobs, are some jobs with the same `stage` value

---

#### Pipeline

A sequence of stages runs sequentially to build, test, deploy applications

Per every commit a pipeline will created, and the all history of runned pipelines are saved in `Gitlab`

![Pipeline](assets/gitlab_pipeline.png)

**Example**:

```yml
build:
    stage: build
    script:
        - npm install
        - npm run build
test_1:
    stage: test
    script:
        - npm run test
test_2:
    stage: test
    script:
        - lint
deploy:
    stage: deploy
    script:
        - npm start
```

<!--prettier-ignore-->
!!! tip "Pipeline Roll back"
    By re-running an specific pipeline we can rollback to that date of server we runned

    ![Pipeline Roll back](assets/gitlab_pipeline_rollback.png)

---

#### Runner

A Server or VM or Container to run a job.

There are two types of runners

1. **Shared Runners**: public and free runners to `build`, `test`
2. **Private Runners**: our own private runners to `build`, `test`, `deploy`

---

### Basic Workflow

1. Use `git-flow`
2. Create `feature/x` branch
3. Develop on local system
4. Merge `feature/x` into `develop` branch
    1. CI/CD deploys `development` environment
5. Merge `develop` branch into `master branch`
    1. CI/CD deploys `staging` environment
    2. CI/CD deploys `production` environment (Optional - Or we deploy it manually)
6. Roll back deploys using re-run old pipelines

---

### Gitlab Validator

Is an online gitlab [tool](https://gitlab.com/tisgroup/dms-server/-/ci/lint) for validating `.gitlab-ci.yml` file before push it

`/Project/Pipelines/CI-Lint`

---

### Gitlab Variables

Some environment variables like `passwords` or `secrets` cannot save on public files in repository, so we must pass them to our application using a secure way

Gitlab allows us to define secret environemnt variables and will pass them to `Runners`

`/Project/Settings/CI-CD/Variables`

---

## Environments

Environemnt is a Hardware and Software configured for a specific target

For example on DevOps we may have 3 environments:

1. Development Server
2. Staging Server
3. Production Server

### Development

A server for development usage (Kamal server in `Farmeal`)

### Staging

A stable with latest features for special users or other developers (Me, Shahin server in `Farmeal`)

### Production

A realy production server for real users (Automatic deploy or Manual deploy)

---

## Artifacts

Every job can create some public file to download, it can use a folder as `artifact` to create the files in that, public

---

## Cache

We can cache a folder on runner for optimize pipeline performance

For example, imagine we have `/node_modules` folder, and per every pipeline for building we must run `npm install`, to prevent this we can cache `/node_modules` folder

---

## Advance Commands

### image

Specify `docker image` used to run the job

#### Pipeline Level

We can specify the default image to run all jobs in that

```yml
image: node:alpine

build:
    # ...
test:
    # ...
deploy:
    # ...
```

#### Job Level

We can specify the job default image to run

```yml
build:
    image: node:alpine
    script:
        # ...
deploy:
    image: nginx:alpine
    # ...
```

---

### before_script/script/after_script

Specify scripts to run on the job

We can specify `before_script` and `after_script` per job or per entire jobs

#### Pipeline Level

Run before/after all jobs

```yml
before_script:
    - npm install
build:
    script:
        - npm run build
after_script:
    - npm run clean
```

#### Job Level

Run before/after a job

```yml
build:
    before_script:
        - npm install
    script:
        - npm run build
    after_script:
        - npm run clean
```

---

### stages/stage

-   **stages**: used to globally define stages and order of them to run
-   **stage**: used to specify a job stage

```yml
stages:
    - build
    - test
    - deploy

job 1:
    stage: build
    script: make build dependencies

job 2:
    stage: build
    script: make build artifacts

job 3:
    stage: test
    script: make test

job 4:
    stage: deploy
    script: make deploy
```

---

### only/except

We can define jobs conditions to run per branch name push

```yml
build:
    stage: build
    script:
        - npm run build
test:
    stage: test
    script:
        - npm run test
deploy:development:
    stage: deploy
    variables:
        PORT: 10000
    script:
        - npm start
    only:
        - develop
deploy:staging:
    stage: deploy
    variables:
        PORT: 9000
    script:
        - npm start
    only:
        - master
deploy:production:
    stage: deploy
    variables:
        PORT: 8000
    script:
        - npm start --production
    only:
        - master
```

---

### allow_failure

If it was true, when the job fails, the pipeline doesn't stop

Default is false per every job

```yml
clean:
    stage: clean
    script:
        - npm run clean
    allow_failure: true
```

---

### tags

Is used to specify which runner to use for running this job

```yml
build:
    stage: build
    tags:
        - build
        - nodejs
```

---

### environment

Is used to specify which environment we want to deploy, used in deploy jobs

```yml
deploy:development:
    stage: development
    environment:
        name: development
        url: http://46.209.98.105:8000/

deploy:staging:
    stage: deploy
    environment:
        name: staging
        url: http://46.209.98.105:9000/
```

#### on_stop/action

We can define handler for on stop in deployment:

```yml
review_app:
    stage: deploy
    script: make deploy-app
    environment:
        name: review
        on_stop: stop_review_app

stop_review_app:
    stage: deploy
    variables:
        GIT_STRATEGY: none
    script: make delete-app
    when: manual
    environment:
        name: review
        action: stop
```

---

### artifacts

We can make some folders publicly downloadable per job or per pipeline

#### Pipeline Level

We can define some global files/folders per pipeline

```yml
artifacts:
    paths:
        - sources/build/
```

#### Job Level

We can define some global files/folders per job

```yml
build:
    stage: build
    artifacts:
        paths:
            - sources/build/
```

---

### variables

We can add environment variables per job or per pipeline

#### Pipeline Level

We can add environment variables per pipeline

```yml
variables:
    DATABASE_URL: "postgres://postgres@postgres/my_database"
    DATABASE_PORT: 3306
```

#### Job Level

We can add environment variables per job

```yml
deploy:
    stage: deploy
    variables:
        DATABASE_URL: "postgres://postgres@postgres/my_database"
        DATABASE_PORT: 3306
```

---

### cache

We can cache some folders for pipeline optimization

```yml
cache:
    paths:
        - sources/node_modules/

build:
    stage: build
    script: npm run build
```

---

# Intro

DevOps is a practice or methodology of making **Developers** and **Operations** folks work together

![DevOps](assets/devops.png)

---

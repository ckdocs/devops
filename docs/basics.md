# Basics

## SDLC Models

SDLC or **Software Development Life Cycle** is a process that consists of a `series of planned activities` to develop or alter the Software Products

There are many SDLC models:

1. **Waterfall**
2. **V-Shaped**
3. **Iterative**
4. **Spiral**
5. **Big Bang**
6. **Agile**

---

### Waterfall

Waterfall is the oldest and most straightforward of the structured SDLC methodologies — finish one phase, then move on to the next. No going back. Each stage relies on information from the previous stage and has its own project plan. Waterfall is easy to understand and simple to manage.

But early delays can throw off the entire project timeline. And since there is little room for revisions once a stage is completed, problems can’t be fixed until you get to the maintenance stage. This model doesn’t work well if flexibility is needed or if the project is long term and ongoing.

![Waterfall](assets/sdlc_waterfall.png)

---

### V-Shaped

Also known as the Verification and Validation model, the V-shaped model grew out of Waterfall and is characterized by a corresponding testing phase for each development stage. Like Waterfall, each stage begins only after the previous one has ended.

This model is useful when there are no unknown requirements, as it’s still difficult to go back and make changes.

![V-Shaped](assets/sdlc_vshaped.jpg)

---

### Iterative

The Iterative model is repetition incarnate. Instead of starting with fully known requirements, you implement a set of software requirements, then test, evaluate and pinpoint further requirements. A new version of the software is produced with each phase, or iteration. Rinse and repeat until the complete system is ready.

One advantage over other SDLC methodologies: This model gives you a working version early in the process and makes it less expensive to implement changes. One disadvantage: Resources can quickly be eaten up by repeating the process again and again.

![Iterative](assets/sdlc_iterative.png)

---

### Sprial

One of the most flexible SDLC methodologies, the Spiral model takes a cue from the Iterative model and its repetition; the project passes through four phases over and over in a “spiral” until completed, allowing for multiple rounds of refinement.

This model allows for the building of a highly customized product, and user feedback can be incorporated from early on in the project. But the risk you run is creating a never-ending spiral for a project that goes on and on.

![Sprial](assets/sdlc_sprial.png)

---

### Big Bang

A bit of an anomaly among SDLC methodologies, the Big Bang model follows no specific process, and very little time is spent on planning. The majority of resources are thrown toward development, and even the client may not have a solid grasp of the requirements. This is one of the SDLC methodologies typically used for small projects with only one or two software engineers.

Big Bang is not recommended for large or complex projects, as it’s a high-risk model; if the requirements are misunderstood in the beginning, you could get to the end and realize the project may have to be started all over again.

![Big Band](assets/sdlc_bigbang.png)

---

### Agile

By breaking the product into cycles, the Agile model quickly delivers a working product and is considered a very realistic development approach. The model produces ongoing releases, each with small, incremental changes from the previous release. At each iteration, the product is tested.

This model emphasizes interaction, as the customers, developers and testers work together throughout the project. But since this model depends heavily on customer interaction, the project can head the wrong way if the customer is not clear on the direction he or she wants to go.

![Agile](assets/sdlc_agile.png)

---

## DevOps vs Agile

<!-- prettier-ignore -->
!!! tip "Agile"
    Agile is a SDLC for removing gaps between **Customers** and **Developers** by creating short time `Sprints` and getting `Feedback` from **Customers** more quickly
    ![Agile](assets/vs_agile.png)

<!-- prettier-ignore -->
!!! tip "DevOps"
    DevOps is a culture for removing gaps between **Developers** and **Operations (IT Managers)** by `Automating` deployment stages
    ![DevOps](assets/vs_devops.png)

---

## DevOps

DevOps comes to remove the gap between **Developers** and **Operations**, operations like:

-   IT Managers (Sys Admins)
-   Testers
-   QA's

![DevOps](assets/devops_2.png)

---

### DevOps Benefits

1. **Predictability**: DevOps offers significantly lower failure rate of new releases
2. **Reproducibility**: Version everything so that earlier version can be restored anytime.
3. **Maintainability**: Effortless process of recovery in the event of a new release crashing or disabling the current system.
4. **Time to market**: DevOps reduces the time to market up to 50% through streamlined software delivery. This is particularly the case for digital and mobile applications.
5. **Greater Quality**: DevOps helps the team to provide improved quality of application development as it incorporates infrastructure issues.
6. **Reduced Risk**: DevOps incorporates security aspects in the software delivery lifecycle. It helps in reduction of defects across the lifecycle.
7. **Resiliency**: The Operational state of the software system is more stable, secure, and changes are auditable.
8. **Cost Efficiency**: DevOps offers cost efficiency in the software development process which is always an aspiration of IT companies' management.
9. **Breaks larger code base into small pieces**: DevOps is based on the agile programming method. Therefore, it allows breaking larger code bases into smaller and manageable chunks.

---

### When and When not to DevOps

-   For **large** applications such as `eCommerce` we should **use** DevOps
-   For **small** applications such as `Wordpress` or simple applications DevOps may increase the time of project

---

### DevOps Lifecycle - Tools

1. **Continuous Development**: Coding
    1. **SCM Tools**
        1. `Gitlab`
        2. `Github`
        3. `Bitbucket`
    2. **Repo Managements**
        1. `NPM`
        2. `Maven`
    3. **Build Tools**
        1. `CMake`
        2. `Gradle`
        3. `Buck`
        4. `Maven`
2. **Continuous Testing**: Testing
    1. **Syntax Testing**: Linting
        1. `ESLint`
        2. `TSLint`
    2. **Unit Testing**: Function
        1. `Jest`
        2. `JUnit`
    3. **Acceptance Testing**: Entire Project
        1. `Selenium`
    4. **Performance Testing**: Load/Stress/Performance
3. **Continuous Integration**: Automating tests, build
    1. `TravisCI`
    2. `CircleCI`
    3. `GitlabCI`
    4. `Jenkins`
4. **Deployment**: Automating deploy, containerization
    1. **Containerization**: Application runtime isolation
        1. `Docker`
        2. `Kubernetes`
    2. **Auto Runner**: Automates running application on server
        1. `Gitlab Runner`
        2. `Jenkins`
    3. **Configure Management**: Automate cluster nodes configurations
        1. `Ansible`
        2. `Puppet`
        3. `Chef`
5. **Monitoring**: Monitoring, feedback (performance)
    1. **Log Management**
        1. `Fluentd`
        2. `ELK Stack`
        3. `Splunk`
    2. **Monitoring**
        1. `Promotheus`
        2. `Nagios`

---

### My DevOps Tools

I will use these tools as my DevOps tools

1. **Continuous Development**
    1. `git`, `git-flow`
2. **Continuous Testing**
    1. `Jest`: JS
    2. `Xunit`: Java, Erlang, PHP, ...
3. **Continuous Integration**
    1. `GitlabCI`: automating `Test`, `Build`
4. **Continuous Deployment**
    1. `GitlabCI`: automating `Deploy` on docker on server
    2. `Docker`: application isolation
    3. `Ansible`: automating servers `Configuration`
5. **Continuous Monitoring**
    1. `Sentry`: issue tracker
    2. `Fluentd`: log management
    3. `Promotheus-Grafana`: monitoring

---
